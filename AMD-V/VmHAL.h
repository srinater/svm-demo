#include "VMCB.h"
/**
* 将虚拟机配置信息写入VMM栈中，并将Vmcb的物理地址作为参数调用vmrun 运行Guest。
* @param Vmcb VMCB
* @param VmmStack Vmm运行的栈内存
* @param VmmConfig 虚拟机配置信息
* @return 此函数永不返回
*/
EXTERN_C VOID __vmrun(PHYSICAL_ADDRESS Vmcb, PSVM_VMM_STACK VmmStack, PSVM_CONFIG VmmConfig);
/**
* 将Host状态恢复为Guest环境，并继续跑下去。
* 其中Guest->Rax = Guest->Rip、Guest->Rcx = Guest->Rsp
* @param GuestFrame Guest环境
* @return 此函数永不返回
*/
EXTERN_C VOID __vmrecover(PSVM_GUEST_FRAME GuestFrame);
/**
* 将当前处理器退出虚拟化环境
*/
VOID __vmcall(VOID);