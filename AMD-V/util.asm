.code
	__sgdt PROC		;save gdt
		sgdt [rcx]
		ret
	__sgdt ENDP
	
	__sidt PROC		;save idt
		sidt [rcx]
		ret
	__sidt ENDP
	
	__sldt PROC		;save ldt selector
		sldt ax
		ret
	__sldt ENDP
	
	__str PROC		;save tr selector
		str ax
		ret
	__str ENDP
end
