#pragma once
#include <ntddk.h>
#pragma pack(push)
#pragma warning(push)

#pragma warning(disable:4201)
#pragma pack(1)

// CPUID 结果位描述
#define CPUID_SVM 2

typedef struct _CPUID_PARAM
{
	int ax;
	int bx;
	int cx;
	int dx;
}CPUID_RESULT, * PCPUID_PARAM;

/**
* 段类型
*/
typedef enum _SEGMENT_TYPE
{
	BasicSegment,
	GdtrSegment,
	IdtrSegment,
	LdtrSegment,
	TrSegment,
	FsSegment,		//See (FS and GS Registers in 64-Bit Mode).
	GsSegment,
}SEGMENT_TYPE, * PSEGMENT_TYPE;

typedef struct _SEGMENT_DESCRIPT {
	union 
	{
		struct
		{
			UINT64 Limit : 16;	// +0x000 bit 
			UINT64 Base : 24;	// +0x010 bit 
			UINT64 Type : 4;	// +0x028 bit	attribute region
			UINT64 S : 1;		// +0x02C bit	
			UINT64 DPL : 2;		// +0x02D bit 
			UINT64 P : 1;		// +0x02F bit	attribute end
			UINT64 LimitHigh : 4;// +0x030 bit	 
			UINT64 AVL : 1;		// +0x034 bit	attribute high region
			UINT64 Reserve : 1;	// +0x035 bit 
			UINT64 D_B : 1;		// +0x036 bit 
			UINT64 G : 1;		// +0x037 bit	attribute high end
			UINT64 BaseHigh : 8;// +0x038 bit 
		};						// size 0x40 bits , 8 bytes
		UINT64 Value;
	};
}SEGMENT_DESCRIPT, * PSEGMENT_DESCRIPT;
static_assert(sizeof(SEGMENT_DESCRIPT) == 0x8, "sizeof(SEGMENT_DESCRIPT) != 0x8");

typedef struct _SEGMENT_ATTRIB {
	union 
	{
		struct 
		{
			UINT16 Type : 4;	// +0x000 bit	attribute region
			UINT16 S : 1;		// +0x004 bit	
			UINT16 DPL : 2;		// +0x005 bit 
			UINT16 P : 1;		// +0x007 bit	attribute end
			UINT16 AVL : 1;		// +0x008 bit	attribute high region
			UINT16 Reserve : 1;	// +0x009 bit 
			UINT16 D_B : 1;		// +0x00A bit 
			UINT16 G : 1;		// +0x00B bit	attribute high end
		};						// size 0x0C bits, 2 bytes
		UINT16	Value;
	};
}SEGMENT_ATTRIB, *PSEGMENT_ATTRIB;
static_assert(sizeof(SEGMENT_ATTRIB) == 0x2, "sizeof(SEGMENT_ATTRIB) != 0x2");

typedef struct _SEGMENT_SELECTOR {
	union
	{
		struct
		{
			UINT16 Rpl : 2;		//Request Privilege-Level
			UINT16 Ti : 1;		//Table Indicator
			UINT16 Index : 13;	//Gdtr index
		};
		UINT16 Value;
	};
}SEGMENT_SELECTOR, * PSEGMENT_SELECTOR;
static_assert(sizeof(SEGMENT_SELECTOR) == 0x2, "sizeof(SEGMENT_SELECTOR) != 0x2");

typedef struct _GDTR{
	UINT16		Limit;
	PSEGMENT_DESCRIPT Base;
}GDTR, *PGDTR, IDTR, PIDTR;
static_assert(sizeof(GDTR) == 10, "sizeof(GDTR) != 10");

#pragma warning(pop)
#pragma pack(pop)

EXTERN_C void __sgdt(_Out_ PGDTR gdtr);
EXTERN_C UINT16 __sldt();
EXTERN_C UINT16 __str();