# SVM-Demo

#### 介绍
一个基础的Windows SVM虚拟化驱动，通过设置拦截修改CPUID_40000000返回信息，务必在虚拟机中运行，支持多处理器。
- Win7x64  通过了测试。
- Win10x64 通过了测试。

#### 软件架构

一个单纯的SVM虚拟化，在加载驱动时将当前系统运行在Guest环境，卸载驱动时将当前系统还原回Host模式。

#### 安装教程

- 驱动没有微软数字签名，需要在没有强制驱动签名，或者处于调试模式中的系统运行。
1.  安装：sc create <服务名> type= kernel start= demand binPath= "sys文件路径"
2.  卸载：sc delete <服务名>

#### 使用说明

1.  加载驱动：net start <服务名>
2.  卸载驱动：net stop <服务名>

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

