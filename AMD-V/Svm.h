#pragma once
#include "VMCB.h"
// CPUID ����
#define CPUID_VMINFO	0x40000000
#define CPUID_VMINFO_AX	'nirS'
#define CPUID_VMINFO_CX	'reta'
#define CPUID_VMINFO_DX	'DMA@'
#define CPUID_VMINFO_BX	'  V-'

BOOLEAN SvmCheckSupport();
BOOLEAN SvmEnable();
VOID SvmDisable();
PSVM_GLOBAL_CONFIG SvmCreateGlobalConfig(UINT32 GuestAsid, SIZE_T VmmStackSize, PSVM_VM_EXIT_HANDLER VmExitHandler);
VOID SvmFreeGlobalConfig(PSVM_GLOBAL_CONFIG GlobalConfig);
BOOLEAN SvmInitConfig(PSVM_GLOBAL_CONFIG GlobalConfig, ULONG ConfigIndex);
VOID SvmClearConfig(PSVM_GLOBAL_CONFIG GlobalConfig, ULONG ConfigIndex);
BOOLEAN SvmCreateVmmStack(PSVM_CONFIG VmConfig, SIZE_T StackSize);
VOID SvmFreeVmmStack(PSVM_CONFIG VmConfig);
BOOLEAN SvmCheckInSvm();
BOOLEAN SvmFillSegmentInfo(PSVM_VMCB_STATE_SAVE_AREA_SEGMENT Segment, SEGMENT_TYPE SegmentType, UINT16 SegSelector);
BOOLEAN SvmContextToVmcb(PSVM_VMCB Vmcb, PCONTEXT CurContext);
BOOLEAN SvmCurrentToVmcb(PSVM_VMCB Vmcb, PUINT64 Rip, PUINT64 Rsp);
BOOLEAN SvmVmmInjectEvent(PSVM_VMCB Vmcb);
BOOLEAN ProcessorVmEntryCallback(PSVM_CONFIG VmConfig);
BOOLEAN ProcessorVmExitCallback(PSVM_CONFIG VmConfig);
VOID SvmVmExitHandler(PSVM_VMM_STACK VmStack);
NTSTATUS SvmVirtualizeProcessor(PSVM_GLOBAL_CONFIG VmGlobalConfig, ULONG ProcessorIndex);
BOOLEAN SvmUnvirtualizeProcessor(ULONG ProcessorIndex);