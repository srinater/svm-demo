.code
	pushaq MACRO	;save state to stack
		pushfq
		push r15
		push r14
		push r13
		push r12
		push r11
		push r10
		push r9
		push r8
		push rbp
		push rdi
		push rsi
		push rdx
		push rbx
		push rcx
		push rax
	ENDM

	popaq MACRO	;recover state from stack
		pop rax
		pop rcx
		pop rbx
		pop rdx
		pop rsi
		pop rdi
		pop rbp
		pop r8
		pop r9
		pop r10
		pop r11
		pop r12
		pop r13
		pop r14
		pop r15
		popfq
	ENDM

	savexmm MACRO
		sub rsp, 060h
		movaps xmm0, xmmword ptr [rsp + 000h]
		movaps xmm1, xmmword ptr [rsp + 010h]
		movaps xmm2, xmmword ptr [rsp + 020h]
		movaps xmm3, xmmword ptr [rsp + 030h]
		movaps xmm4, xmmword ptr [rsp + 040h]
		movaps xmm5, xmmword ptr [rsp + 050h]
	ENDM

	loadxmm MACRO
		movaps  [rsp + 000h], xmm0
		movaps  [rsp + 010h], xmm1
		movaps  [rsp + 020h], xmm2
		movaps  [rsp + 030h], xmm3
		movaps  [rsp + 040h], xmm4
		movaps  [rsp + 050h], xmm5
		add rsp, 060h
	ENDM

	;[[noreturn]] VOID __vmrun(PHYSICAL_ADDRESS Vmcb, PVMSTACK VmmStack, PVMCONFIG VmmConfig)
	; stack(VmmStack):
	; pushaq	0x80
	; savexmm	0x60
	; &VmmState	0x08
	; Vmcb		0x08
	;
	__vmrun PROC
		mov rax, rcx
		mov rsp, rdx
		push rcx
		push r8

start_guest:
		vmrun rax
		mov rax, [rsp]
		pushaq
		savexmm

		mov rcx, rsp
		call qword ptr [rax]	;VmState->Rip
		loadxmm					;pop xmm
		popaq					;popaq
		mov rax, [rsp + 08h]	;get Vmcb
		jmp	start_guest
	__vmrun ENDP

	;[[noreturn]] VOID __vmrecover(PGUEST_FRAME GuestFrame);
	;
	; use: guest rcx, rax
	; Guest->Rax = Guest->Rip
	; Guest->Rcx = Guest->Rsp
	; 
	__vmrecover PROC
		mov rsp, rcx			; 挂接到Vmm栈上，导出所有寄存器
		loadxmm					; pop xmm
		popaq					; popaq
		mov rsp, rcx			; Guest->Rcx
		xor rcx, rcx			;
		push rax
		xor rax, rax
		ret						; jmp rax
	__vmrecover ENDP
	; VOID __vmcall(VOID);
	; 将当前处理器退出虚拟化环境
	; 暂时没有参数，也没有返回信息
	;
	__vmcall PROC
		push rax
		push rcx
		vmmcall
		pop rcx
		pop rax
		ret
	__vmcall ENDP
end
