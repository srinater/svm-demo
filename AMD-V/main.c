#include "VmExitCode.h"
#include "Svm.h"
#include "VmHAL.h"

#define VMM_STACK_SZIE (PAGE_SIZE * 3)

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD DriverUnload;
SVM_VM_EXIT_HANDLER SvmVmExitHandler;

PSVM_GLOBAL_CONFIG GlobalConfig = NULL;

/**
* 驱动入口函数
* @param driverObject 驱动对象
* @param unicodeString 驱动注册表路径
* @return 驱动加载状态
*/
NTSTATUS DriverEntry(_In_ PDRIVER_OBJECT driverObject, _In_ PUNICODE_STRING unicodeString)
{
	UNREFERENCED_PARAMETER(driverObject);
	UNREFERENCED_PARAMETER(unicodeString);
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	driverObject->DriverUnload = DriverUnload;

	ULONG ProcIndex = 0;
	GlobalConfig = SvmCreateGlobalConfig(1, VMM_STACK_SZIE, SvmVmExitHandler);
	if (!GlobalConfig)
		goto failed;

	ULONG ProcCount = KeQueryActiveProcessorCountEx(ALL_PROCESSOR_GROUPS);
	for (ProcIndex ; ProcIndex < ProcCount; ++ProcIndex)
	{
		Status = SvmVirtualizeProcessor(GlobalConfig, ProcIndex);
		if (!NT_SUCCESS(Status))
			goto failed;
	}
	return Status;
failed:
	while (ProcIndex)
	{
		--ProcIndex;
		if (!SvmUnvirtualizeProcessor(ProcIndex))
			NT_ASSERT(FALSE);		// 无法卸载？！
	}
	if (GlobalConfig)
		SvmFreeGlobalConfig(GlobalConfig);
	return Status;
}
/**
* 驱动卸载函数
* @param driverObject 驱动对象
*/
VOID DriverUnload(_In_ PDRIVER_OBJECT driverObject)
{
	UNREFERENCED_PARAMETER(driverObject);
	ULONG ProcCount = KeQueryActiveProcessorCountEx(ALL_PROCESSOR_GROUPS);
	for (ULONG ProcIndex = 0; ProcIndex < ProcCount; ++ProcIndex)
	{
		if (!SvmUnvirtualizeProcessor(ProcIndex))
			NT_ASSERT(FALSE);		// 无法卸载？！
	}
	if (GlobalConfig)
		SvmFreeGlobalConfig(GlobalConfig);
}

/**
* # VMEXIT 处理函数
* @param VmStack 包含Guest 执行状态以及VM配置信息。
*/
VOID SvmVmExitHandler(PSVM_VMM_STACK VmStack)
{
	PSVM_VMCB Vmcb = VmStack->VmConfig->Vmcb;
	switch (Vmcb->ControlArea.ExitCode)
	{
	break;  case VMEXIT_INVALID:
		// 配置错误
		NT_ASSERT(FALSE);
	break;  case VMEXIT_CPUID:
		if (Vmcb->StateSaveArea.Rax == CPUID_VMINFO /*&& !Vmcb->StateSaveArea.Cpl*/)
		{
			Vmcb->StateSaveArea.Rax = CPUID_VMINFO_AX;
			VmStack->Frame.rcx = CPUID_VMINFO_CX;
			VmStack->Frame.rdx = CPUID_VMINFO_DX;
			VmStack->Frame.rbx = CPUID_VMINFO_BX;
			Vmcb->StateSaveArea.Rip = Vmcb->ControlArea.NRip;
		}
		else {
			CPUID_RESULT cpuidResult;
			__cpuidex((int*)&cpuidResult, (int)Vmcb->StateSaveArea.Rax, (int)VmStack->Frame.rcx);
			Vmcb->StateSaveArea.Rax = cpuidResult.ax;
			VmStack->Frame.rcx = cpuidResult.cx;
			VmStack->Frame.rbx = cpuidResult.bx;
			VmStack->Frame.rdx = cpuidResult.dx;
			Vmcb->StateSaveArea.Rip = Vmcb->ControlArea.NRip;
		}
	break; case VMEXIT_VMMCALL:
	{
		Vmcb->StateSaveArea.Rip = Vmcb->ControlArea.NRip;
		if (!Vmcb->StateSaveArea.Cpl) {
			VmStack->Frame.rax = Vmcb->StateSaveArea.Rip;
			VmStack->Frame.rcx = Vmcb->StateSaveArea.Rsp;
			__svm_stgi();
			SvmDisable();
			ProcessorVmExitCallback(VmStack->VmConfig);
			__vmrecover(&VmStack->Frame);
			//此函数永不返回
			NT_ASSERT(FALSE);
		}
	}
	break; default:
		// 是不是忘记处理什么了
		NT_ASSERT(FALSE);
	}
}
