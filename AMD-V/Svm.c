#include <ntifs.h>
#include "msr.h"
#include "VMCB.h"

#include "Svm.h"
#include "VmHAL.h"

#define SVM_POOL_TAG 'bcmv'

/**
* 检查当前CPU是否支持SVM
* @return 支持与否
*/
BOOLEAN SvmCheckSupport()
{
	// 检测是否为AMD CPU，同时检测cpuid是否支持指令扩展
	CPUID_RESULT cpuidResult;
	__cpuid((int*)&cpuidResult, 0x80000000);
	if (
		cpuidResult.bx != 0x68747541
		|| cpuidResult.dx != 0x69746E65
		|| cpuidResult.cx != 0x444D4163
		|| cpuidResult.ax <= 0x80000000
		)
		return FALSE;

	// 检测CPU是否支持SVM
	__cpuid((int*)&cpuidResult, 0x80000001);
	if (((cpuidResult.cx >> CPUID_SVM) & 1) == 0)
		return FALSE;

	// 检测SVM当前是否可用，更多原因参考CPUID Fn8000_000A_EDX[SVML]
	ULONGLONG msrData = __readmsr(MSR_VM_CR);
	if ((msrData >> MSR_VM_CR_SVMDIS) & 1)
		return FALSE;

	return TRUE;
}

/**
* 启用当前处理器的SVM
* @return 成功或失败
*/
BOOLEAN SvmEnable()
{
	ULONGLONG Efer = __readmsr(MSR_EFER);
	if ((Efer >> MSR_EFER_SVME) & 1)
		return FALSE;
	__writemsr(MSR_EFER, Efer | (1ULL << MSR_EFER_SVME));
	return TRUE;
}

/**
* 禁用当前处理器的SVM
*/
VOID SvmDisable()
{
	ULONGLONG Efer = __readmsr(MSR_EFER);
	__writemsr(MSR_EFER, Efer & ~(1ULL << MSR_EFER_SVME));
}

/**
* 创建并初始化全局配置信息，将按照当前CPU核心数分配相应的vCpu配置信息内存，并初始化它们的Vmm栈，MSR_VM_HSAVE_PA内存，Vmcb配置等
* @param GuestAsid 当前虚拟机使用的独立ID，用于区分页表
* @param VmmStackSize VMM栈大小
* @param VnExitHandler #VMEXIT的处理函数
* @return 成功时返回初始化的配置信息，失败时返回NULL
*/
PSVM_GLOBAL_CONFIG SvmCreateGlobalConfig(UINT32 GuestAsid, SIZE_T VmmStackSize, PSVM_VM_EXIT_HANDLER VmExitHandler)
{
	// 分配全局配置信息
	PSVM_GLOBAL_CONFIG GlobalConfig = NULL;
	PSVM_CONFIG SvmConfigs = NULL;
	
	GlobalConfig = (PSVM_GLOBAL_CONFIG)ExAllocatePoolWithTag(NonPagedPool, sizeof(SVM_GLOBAL_CONFIG), SVM_POOL_TAG);
	if (!GlobalConfig)
		goto failed;

	RtlZeroMemory(GlobalConfig, sizeof(SVM_GLOBAL_CONFIG));
	
	// 分配HostSave状态保存
	ULONG ProcCount = KeQueryActiveProcessorCountEx(ALL_PROCESSOR_GROUPS);
	SvmConfigs = ExAllocatePoolWithTag(NonPagedPool, ProcCount * sizeof(SVM_CONFIG), SVM_POOL_TAG);
	if (!SvmConfigs)
		goto failed;
	RtlZeroMemory(SvmConfigs, ProcCount * sizeof(SVM_CONFIG));

	GlobalConfig->VMExitFunction = VmExitHandler;
	GlobalConfig->GuestAsid = GuestAsid;
	GlobalConfig->VmmStackSize = VmmStackSize;
	GlobalConfig->ConfigList = SvmConfigs;
	GlobalConfig->ConfigActiveCount = 0;	// 配置成功才计数
	GlobalConfig->ConfigCount = ProcCount;

	/**
	* 初始化所有vCPU核心配置
	*/
	ULONG ProcIndex;
	for (ProcIndex = 0; ProcIndex < ProcCount; ++ProcIndex)
		if (!SvmInitConfig(GlobalConfig, ProcIndex))
			break;

	GlobalConfig->ConfigActiveCount = ProcIndex;
	if (ProcIndex != ProcCount) // 存在配置错误
		goto failed;
	
	return GlobalConfig;
failed:
	if (GlobalConfig)
		SvmFreeGlobalConfig(GlobalConfig);
	return NULL;
}

/**
* 释放全局虚拟化配置信息，以及相应的所有vCpu配置信息。
* @param GlobalConfig 全局虚拟化配置信息
*/
VOID SvmFreeGlobalConfig(PSVM_GLOBAL_CONFIG GlobalConfig)
{
	ASSERT(GlobalConfig);
	ULONG ProcIndex;
	if (GlobalConfig->ConfigActiveCount)
	{
		for (ProcIndex = 0; ProcIndex < GlobalConfig->ConfigActiveCount; ++ProcIndex)
		{
			SvmClearConfig(GlobalConfig, ProcIndex);
		}
		ExFreePoolWithTag(GlobalConfig->ConfigList, SVM_POOL_TAG);
	}
	ExFreePoolWithTag(GlobalConfig, SVM_POOL_TAG);
}

/**
* 初始化虚拟机配置信息（当前CPU）,包括Vmm栈，MSR_VM_HSAVE_PA内存，Vmcb等
* @param GlobalConfig 全局虚拟化配置信息
* @param ConfigIndex 需要初始化配置信息的vCpu索引
* @return 成功时新分配的虚拟机配置信息，失败时返回NULL
*/
BOOLEAN SvmInitConfig(PSVM_GLOBAL_CONFIG GlobalConfig, ULONG ConfigIndex)
{
	PSVM_CONFIG Config = NULL;
	PSVM_VMCB Vmcb = NULL;
	PVOID VmHostSave = NULL;
	if (ConfigIndex >= GlobalConfig->ConfigCount)
	{
		NT_ASSERT(FALSE);
		//return FALSE;
	}

	Config = &GlobalConfig->ConfigList[ConfigIndex];
	
	// 分配VMCB
	Vmcb = ExAllocatePoolWithTag(NonPagedPool, sizeof(SVM_VMCB), SVM_POOL_TAG);
	if (!Vmcb)
		goto failed;

	RtlZeroMemory(Vmcb, sizeof(SVM_VMCB));
	// 初始化VMCB->GuestAsid
	Vmcb->ControlArea.GuestAsid = GlobalConfig->GuestAsid;

	/**
	* 初始化VmConfig
	*/
	Config->Vmcb = Vmcb;
	Config->VmcbPA = MmGetPhysicalAddress(Vmcb);
	Config->VmExitFunction = GlobalConfig->VMExitFunction;

	VmHostSave = ExAllocatePoolWithTag(NonPagedPool, PAGE_SIZE, SVM_POOL_TAG);			// 为MSR_VM_HSAVE_PA分配内存
	if (!VmHostSave)
		goto failed;

	RtlZeroMemory(VmHostSave, PAGE_SIZE);
	Config->VmHostSave = VmHostSave;
	Config->VmHostSavePA = MmGetPhysicalAddress(VmHostSave);
	Config->GlobalConfig = GlobalConfig;												// 链接全局配置

	if (!SvmCreateVmmStack(Config, GlobalConfig->VmmStackSize))
		goto failed;

	return TRUE;

failed:
	if (Vmcb)
		SvmClearConfig(GlobalConfig, ConfigIndex);

	return FALSE;
}

/**
* 释放虚拟机配置信息的内存，包括Vmm栈
* @param GlobalConfig 全局虚拟化配置信息
* @param ConfigIndex 需要清除配置信息的vCpu索引
*/
VOID SvmClearConfig(PSVM_GLOBAL_CONFIG GlobalConfig, ULONG ConfigIndex)
{
	NT_ASSERT(GlobalConfig);
	if (ConfigIndex >= GlobalConfig->ConfigCount) {
		NT_ASSERT(FALSE);
		return;
	}
	PSVM_CONFIG Config = &GlobalConfig->ConfigList[ConfigIndex];
	SvmFreeVmmStack(Config);
	if (Config->VmHostSave) {
		ExFreePoolWithTag(Config->VmHostSave, SVM_POOL_TAG);
		Config->VmHostSave = NULL;
		Config->VmHostSavePA.QuadPart = 0;
	}
	if (Config->Vmcb)
	{
		ExFreePoolWithTag(Config->Vmcb, SVM_POOL_TAG);
		Config->Vmcb = NULL;
		Config->VmcbPA.QuadPart = 0;
	}
	Config->VmExitFunction = NULL;
	Config->GlobalConfig = NULL;
}

/**
* 创建Vmm使用的栈，并将栈信息写入VmConfig，此时栈是清零的。
* @param VmConfig 虚拟机配置信息
* @param StackSize Vmm栈大小
* @return 创建是否成功
*/
BOOLEAN SvmCreateVmmStack(PSVM_CONFIG VmConfig, SIZE_T StackSize)
{
	ASSERT(VmConfig != NULL);

	StackSize = (UINT64)PAGE_ALIGN((PVOID)StackSize);
	if (StackSize < PAGE_SIZE)
		StackSize = PAGE_SIZE;

	PUINT8 StackBase = (PUINT8)ExAllocatePoolWithTag(NonPagedPool, StackSize, SVM_POOL_TAG);
	if (!StackBase)
		return FALSE;

	PUINT8 StackTop = StackBase + StackSize;
	PSVM_VMM_STACK VmStack = (PSVM_VMM_STACK)(StackTop - sizeof(SVM_VMM_STACK));
	VmStack->VmConfig = VmConfig;
	VmStack->CurrentVmcbPA = VmConfig->VmcbPA;
	VmConfig->VmmStackTop = StackTop;
	VmConfig->VmmStackBase = StackBase;
	VmConfig->VmmStackSize = StackSize;
	return TRUE;
}
/**
* 销毁并释放VMM栈。
* @param VmConfig 需要释放VMM栈对应的vCpu配置信息
* @return 是否在VM环境中
*/
VOID SvmFreeVmmStack(PSVM_CONFIG VmConfig)
{
	NT_ASSERT(VmConfig);
	if (VmConfig->VmmStackBase)
	{
		ExFreePoolWithTag(VmConfig->VmmStackBase, SVM_POOL_TAG);
		VmConfig->VmmStackBase = NULL;
		VmConfig->VmmStackTop = NULL;
		VmConfig->VmmStackSize = 0;
	}
}
/**
* 用于确认是否在VM环境中
* @return 是否在VM环境中
*/
BOOLEAN SvmCheckInSvm()
{
	CPUID_RESULT result;
	__cpuid((int*)&result, CPUID_VMINFO);
	return result.ax == CPUID_VMINFO_AX
		&& result.cx == CPUID_VMINFO_CX
		&& result.dx == CPUID_VMINFO_DX
		&& result.bx == CPUID_VMINFO_BX;
}

/**
* 通过段类型与段选择子获取段信息，并填充到Vmcb的段信息结构中。
* @param Segment Vmcb中段信息结构体
* @param SegmentType 枚举类型SEGMENT_TYPE，指定需要填充的段类型。
* @param SegSelector 段选择子，段类型无选择子将忽略该参数。
* @return 是否查找到段信息并填充
*/
BOOLEAN SvmFillSegmentInfo(PSVM_VMCB_STATE_SAVE_AREA_SEGMENT Segment, SEGMENT_TYPE SegmentType, UINT16 SegSelector)
{
	switch (SegmentType)
	{
	break;  case BasicSegment:
	{
		GDTR gdtr;
		__sgdt(&gdtr);
		SEGMENT_SELECTOR selector;
		selector.Value = SegSelector;
		// Is null selector (See 4.6.1 Global Descriptor Table)
		if (!selector.Ti && !selector.Index)
			return FALSE;

		PSEGMENT_DESCRIPT curSel = &gdtr.Base[selector.Index];
		Segment->Selector.Value = SegSelector;
		Segment->Attrib.AVL = (UINT16)curSel->AVL;
		Segment->Attrib.DPL = (UINT16)curSel->DPL;
		Segment->Attrib.D_B = (UINT16)curSel->D_B;
		Segment->Attrib.G = (UINT16)curSel->G;
		Segment->Attrib.P = (UINT16)curSel->P;
		Segment->Attrib.Reserve = (UINT16)curSel->Reserve;
		Segment->Attrib.S = (UINT16)curSel->S;
		Segment->Attrib.Type = (UINT16)curSel->Type;
		Segment->Base = curSel->Base | (curSel->BaseHigh << 24);
		Segment->Limit = __segmentlimit(SegSelector);
	}
	break; case GdtrSegment:
	{
		GDTR gdtr;
		__sgdt(&gdtr);
		Segment->Base = (UINT64)gdtr.Base;
		Segment->Attrib.Value = 0;
		Segment->Limit = gdtr.Limit;
		Segment->Selector.Value = 0;
	}
	break; case IdtrSegment:
	{
		IDTR idtr;
		__sidt(&idtr);
		Segment->Base = (UINT64)idtr.Base;
		Segment->Attrib.Value = 0;
		Segment->Limit = idtr.Limit;
		Segment->Selector.Value = 0;
	}
	break; case FsSegment:
	{
		// Use Basic Segment
	}
	break; case LdtrSegment:
	{
		//__sldt();
		return FALSE;
	}
	break; case TrSegment:
	{
		//__str();
		return FALSE;
	}
	break; default:
		return FALSE;
	}
	return TRUE;
}
/**
* 将上下文信息设置到Vmcb中。
* @param Vmcb 初始化后的Vmcb
* @param CurContext 需要引用的Context
* @return 是否填充成功
*/
BOOLEAN SvmContextToVmcb(PSVM_VMCB Vmcb, PCONTEXT CurContext)
{
	SvmFillSegmentInfo(&Vmcb->StateSaveArea.EsSegment, BasicSegment, CurContext->SegEs);
	SvmFillSegmentInfo(&Vmcb->StateSaveArea.CsSegment, BasicSegment, CurContext->SegCs);
	SvmFillSegmentInfo(&Vmcb->StateSaveArea.SsSegment, BasicSegment, CurContext->SegSs);
	SvmFillSegmentInfo(&Vmcb->StateSaveArea.DsSegment, BasicSegment, CurContext->SegDs);
	SvmFillSegmentInfo(&Vmcb->StateSaveArea.GdtrSegment, GdtrSegment, 0);
	SvmFillSegmentInfo(&Vmcb->StateSaveArea.IdtrSegment, IdtrSegment, 0);

	Vmcb->StateSaveArea.Efer = __readmsr(MSR_EFER);
	Vmcb->StateSaveArea.Cr0 = __readcr0();
	Vmcb->StateSaveArea.Cr4 = __readcr4();
	Vmcb->StateSaveArea.Cr3 = __readcr3();
	Vmcb->StateSaveArea.Cr2 = __readcr2();
	Vmcb->StateSaveArea.Rflags = CurContext->EFlags;
	Vmcb->StateSaveArea.Rip = CurContext->Rip;
	Vmcb->StateSaveArea.Rsp = CurContext->Rsp;
	Vmcb->StateSaveArea.Rax = CurContext->Rax;
	Vmcb->StateSaveArea.Dr7 = 0x400;
	Vmcb->StateSaveArea.Dr6 = 0xFFFF0FF0;
	Vmcb->StateSaveArea.Cpl = 0;
	Vmcb->StateSaveArea.GPat = __readmsr(MSR_PAT);

	return TRUE;
}

/**
* 将当前环境用于初始化Vmcb，并要求设置新的Rip与Rsp。
* @param Vmcb 要设置的Vmcb
* @param Rip 新的Rip
* @param Rsp 新的Rsp
*/
BOOLEAN SvmCurrentToVmcb(PSVM_VMCB Vmcb, PUINT64 Rip, PUINT64 Rsp)
{
	CONTEXT CurContext;
	RtlCaptureContext(&CurContext);
	CurContext.Rip = (ULONG64)Rip;
	CurContext.Rsp = (ULONG64)Rsp;
	return SvmContextToVmcb(Vmcb, &CurContext);
}

BOOLEAN SvmVmmInjectEvent(PSVM_VMCB Vmcb)
{
	UNREFERENCED_PARAMETER(Vmcb);
	//TODO:
	return FALSE;
}

/**
* 在执行Vmrun之前最后的处理。
* @param VmConfig 当前vCpu的配置信息
* @return 是否成功，并允许执行vmrun
*/
BOOLEAN ProcessorVmEntryCallback(PSVM_CONFIG VmConfig)
{
	PSVM_VMCB Vmcb = VmConfig->Vmcb;
	Vmcb->ControlArea.InterceptMisc1.CPUID = TRUE;
	Vmcb->ControlArea.InterceptMisc2.VMRUN = TRUE;
	Vmcb->ControlArea.InterceptMisc2.VMCALL = TRUE;
	__writemsr(MSR_VM_HSAVE_PA, VmConfig->VmHostSavePA.QuadPart);
	return TRUE;
}

/**
* 在退出Guest之后做的处理。
* @param VmConfig 当前vCpu的配置信息
* @return 是否处理成功
*/
BOOLEAN ProcessorVmExitCallback(PSVM_CONFIG VmConfig)
{
	UNREFERENCED_PARAMETER(VmConfig);
	__writemsr(MSR_VM_HSAVE_PA, 0);
	return TRUE;
}

/**
* 将指定的vCpu退出虚拟化环境
* @param ProcessorIndex 指定退出虚拟化的vCpu索引
* @return 是否处理成功
*/
BOOLEAN SvmUnvirtualizeProcessor(ULONG ProcessorIndex)
{
	PROCESSOR_NUMBER ProcNumber;
	KeGetProcessorNumberFromIndex(ProcessorIndex, &ProcNumber);
	GROUP_AFFINITY OldGroupAffinity;
	GROUP_AFFINITY GroupAffinity;
	RtlZeroMemory(&OldGroupAffinity, sizeof(GROUP_AFFINITY));
	RtlZeroMemory(&GroupAffinity, sizeof(GROUP_AFFINITY));

	GroupAffinity.Group = ProcNumber.Group;
	GroupAffinity.Mask = 1ULL << ProcNumber.Number;
	KeSetSystemGroupAffinityThread(&GroupAffinity, &OldGroupAffinity);

	if (SvmCheckInSvm())
	{
		__vmcall();
		KdPrint(("Exited!"));
	}
	KeRevertToUserGroupAffinityThread(&OldGroupAffinity);
	return TRUE;
}
/**
* 虚拟化指定索引的CPU
* @param VmGlobalConfig 全局虚拟化配置信息
* @param ProcessorIndex 指定CPU的索引
* @return 虚拟化执行后结果
*/
NTSTATUS SvmVirtualizeProcessor(PSVM_GLOBAL_CONFIG VmGlobalConfig, ULONG ProcessorIndex)
{
	NTSTATUS result = STATUS_UNSUCCESSFUL;
	BOOLEAN isEnableVmx = FALSE;

	PSVM_CONFIG VmConfig = &VmGlobalConfig->ConfigList[ProcessorIndex];
	PSVM_VMCB Vmcb = VmConfig->Vmcb;
	
	/**
	* 运行在指定的核心上
	*/
	PROCESSOR_NUMBER ProcNumber;
	KeGetProcessorNumberFromIndex(ProcessorIndex, &ProcNumber);
	GROUP_AFFINITY OldGroupAffinity;
	GROUP_AFFINITY GroupAffinity;
	RtlZeroMemory(&OldGroupAffinity, sizeof(GROUP_AFFINITY));
	RtlZeroMemory(&GroupAffinity, sizeof(GROUP_AFFINITY));

	GroupAffinity.Group = ProcNumber.Group;
	GroupAffinity.Mask = 1ULL << ProcNumber.Number;
	KeSetSystemGroupAffinityThread(&GroupAffinity, &OldGroupAffinity);
	/**
	* 检查运行环境
	*/
	if (!SvmCheckSupport())
	{
		KdPrint(("CPU not support!"));
		goto failed;
	}
	if (!SvmEnable())
	{
		KdPrint(("AMD-V in used!"));
		goto failed;
	}
	isEnableVmx = TRUE;

	/**
	* 获得当前上下文，Guest将从SvmCheckInSvm处开始执行。
	*/
	CONTEXT CurContext;
	RtlCaptureContext(&CurContext);
	if (!SvmCheckInSvm())
	{
		if (!SvmContextToVmcb(Vmcb, &CurContext))
		{
			KdPrint(("Init VMCB failed!"));
			goto failed;
		}
		if (!ProcessorVmEntryCallback(VmConfig))
		{
			KdPrint(("Callback failed!"));
			goto failed;
		}
		__vmrun(VmConfig->VmcbPA, VmConfig->VmmStackTop, VmConfig);
		//此函数永不返回
		NT_ASSERT(FALSE);
	}
	KeRevertToUserGroupAffinityThread(&OldGroupAffinity);
	return STATUS_SUCCESS;

failed:
	// Recover environment
	if (isEnableVmx)
		SvmDisable();

	KeRevertToUserGroupAffinityThread(&OldGroupAffinity);
	return result;
}
