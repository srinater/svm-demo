#pragma once
#include <stddef.h>
#include "util.h"
#pragma pack(push)
#pragma pack(1)

typedef struct _SVM_VMCB_INTERCEPT_MISC1
{
    UINT32 INTR : 1;
    UINT32 NMI : 1;
    UINT32 SMI : 1;
    UINT32 INIT : 1;
    UINT32 VINTR : 1;
    UINT32 CR0NotTSOrMP : 1;
    UINT32 ReadIDTR : 1;
    UINT32 ReadGDTR : 1;
    UINT32 ReadLDTR : 1;
    UINT32 ReadTR : 1;
    UINT32 WriteIDTR : 1;
    UINT32 WriteGDTR : 1;
    UINT32 WriteLDTR : 1;
    UINT32 WriteTR : 1;
    UINT32 RDTSC : 1;
    UINT32 RDPMC : 1;
    UINT32 PUSHF : 1;
    UINT32 POPF : 1;
    UINT32 CPUID : 1;
    UINT32 RSM : 1;
    UINT32 IRET : 1;
    UINT32 INTn : 1;
    UINT32 INVD : 1;
    UINT32 PAUSE : 1;
    UINT32 HLT : 1;
    UINT32 INVLPG : 1;
    UINT32 INVLPGA : 1;
    UINT32 IOIO_PORT : 1;
    UINT32 MSR_PORT : 1;
    UINT32 TaskSwitch : 1;
    UINT32 FERR_FREEZE : 1;
    UINT32 Shutdown : 1;
}SVM_VMCB_INTERCEPT_MISC1, *PSVM_VMCB_INTERCEPT_MISC1;
static_assert(sizeof(SVM_VMCB_INTERCEPT_MISC1) == sizeof(UINT32), "sizeof(SVM_VMCB_INTERCEPT_MISC1) != sizeof(UINT32)");

typedef struct _SVM_VMCB_INTERCEPT_MISC2
{
    UINT32 VMRUN : 1;           //必须设置的位
    UINT32 VMCALL: 1;
    UINT32 VMLOAD: 1;
    UINT32 VMSAVE: 1;
    UINT32 STGI : 1;
    UINT32 CLGI : 1;
    UINT32 SKINIT : 1;
    UINT32 RDTSCP : 1;
    UINT32 ICEBP : 1;
    UINT32 WBINVD_WBNOINVD : 1;
    UINT32 MONITOR_X : 1;
    UINT32 MWAIT_X : 1;
    UINT32 MWAIT_X_IsMonitored : 1;
    UINT32 XSETBV : 1;
    UINT32 RDPRU : 1;
    UINT32 EFER_WriteFinishes : 1;
    UINT32 CR_WriteFinishes : 16;
}SVM_VMCB_INTERCEPT_MISC2, * PSVM_VMCB_INTERCEPT_MISC2;
static_assert(sizeof(SVM_VMCB_INTERCEPT_MISC2) == sizeof(UINT32), "sizeof(SVM_VMCB_INTERCEPT_MISC2) != sizeof(UINT32)");

typedef struct _SVM_VMCB_INTERCEPT_MISC3
{
    UINT32 INVLPGBAll: 1;
    UINT32 INVLPGBOnlyIllegally: 1;
    UINT32 INVPCID: 1;
    UINT32 MCOMMIT: 1;
    UINT32 TLBSYNC: 1;
    UINT32 ReserveZ : 27;
}SVM_VMCB_INTERCEPT_MISC3, * PSVM_VMCB_INTERCEPT_MISC3;
static_assert(sizeof(SVM_VMCB_INTERCEPT_MISC3) == sizeof(UINT32), "sizeof(SVM_VMCB_INTERCEPT_MISC3) != sizeof(UINT32)");

typedef enum _SVM_VMCB_EVENT_TYPE
{
    INTR = 0,       // 硬件可屏蔽中断
    NMI = 2,        // 不可屏蔽中断
    EXCEPTION = 3,  // 异常
    INTn = 4,       // 软件模拟中断
}SVM_VMCB_EVENT_TYPE, *PSVM_VMCB_EVENT_TYPE;

typedef struct _SVM_VMCB_EVENT_INJ{
    UINT64 Vector : 8;  //中断或者异常的IDT向量
    UINT64 Type : 3;    //enum VMCB_EVENT_TYPE type
    UINT64 EV : 1;      //如果是异常应该把错误退到堆栈上
    UINT64 ReserveZ : 19;
    UINT64 Vailed : 1;  // 有效位
    UINT64 ErrorCode : 32;// 错误代码
}SVM_VMCB_EVENT_INJ, *PSVM_VMCB_EVENT_INJ;
static_assert(sizeof(SVM_VMCB_EVENT_INJ) == sizeof(UINT64), "sizeof(SVM_VMCB_EVENT_INJ) != sizeof(UINT64)");

typedef struct _SVM_VMCB_CONTROL_AREA
{
    UINT16 InterceptCrReadMask;         // +0x000   CR0-15
    UINT16 InterceptCrWriteMask;        // +0x002   CR0-15
    UINT16 InterceptDrReadMask;         // +0x004   DR0-15
    UINT16 InterceptDrWriteMask;        // +0x006   DR0-15
    UINT32 InterceptExceptionMask;      // +0x008   异常向量0-31
    SVM_VMCB_INTERCEPT_MISC1 InterceptMisc1;// +0x00c   
    SVM_VMCB_INTERCEPT_MISC2 InterceptMisc2;// +0x010
    SVM_VMCB_INTERCEPT_MISC3 InterceptMisc3;// +0x014
    UINT8 Reserved1[0x03c - 0x018];     // +0x018
    UINT16 PauseFilterThreshold;        // +0x03c
    UINT16 PauseFilterCount;            // +0x03e
    UINT64 IopmBasePa;                  // +0x040
    UINT64 MsrpmBasePa;                 // +0x048
    UINT64 TscOffset;                   // +0x050
    UINT32 GuestAsid;                   // +0x058
    UINT32 TlbControl;                  // +0x05c
    UINT64 VIntr;                       // +0x060
    UINT64 InterruptShadow;             // +0x068
    UINT64 ExitCode;                    // +0x070
    UINT64 ExitInfo1;                   // +0x078
    UINT64 ExitInfo2;                   // +0x080
    UINT64 ExitIntInfo;                 // +0x088
    UINT64 NpEnable;                    // +0x090
    UINT64 AvicApicBar;                 // +0x098
    UINT64 GuestPaOfGhcb;               // +0x0a0
    UINT64 EventInj;                    // +0x0a8
    UINT64 NCr3;                        // +0x0b0
    UINT64 LbrVirtualizationEnable;     // +0x0b8
    UINT64 VmcbClean;                   // +0x0c0
    UINT64 NRip;                        // +0x0c8
    UINT8 NumOfBytesFetched;            // +0x0d0
    UINT8 GuestInstructionBytes[15];    // +0x0d1
    UINT64 AvicApicBackingPagePointer;  // +0x0e0
    UINT64 Reserved2;                   // +0x0e8
    UINT64 AvicLogicalTablePointer;     // +0x0f0
    UINT64 AvicPhysicalTablePointer;    // +0x0f8
    UINT64 Reserved3;                   // +0x100
    UINT64 VmcbSaveStatePointer;        // +0x108
    UINT8 Reserved4[0x400 - 0x110];     // +0x110
} SVM_VMCB_CONTROL_AREA, * PSVM_VMCB_CONTROL_AREA;
static_assert(sizeof(SVM_VMCB_CONTROL_AREA) == 0x400, "sizeof(SVM_VMCB_CONTROL_AREA) != 0x400");

typedef struct _SVM_VMCB_STATE_SAVE_AREA_SEGMENT {
    SEGMENT_SELECTOR Selector;        // +0x000
    SEGMENT_ATTRIB Attrib;            // +0x002
    UINT32 Limit;                     // +0x004
    UINT64 Base;                      // +0x008
}SVM_VMCB_STATE_SAVE_AREA_SEGMENT, *PSVM_VMCB_STATE_SAVE_AREA_SEGMENT;
static_assert(sizeof(SVM_VMCB_STATE_SAVE_AREA_SEGMENT) == 0x10, "sizeof(PVMCB_STATE_SAVE_AREA_SEGMENT) != 0x10");

typedef struct _SVM_VMCB_STATE_SAVE_AREA
{
    SVM_VMCB_STATE_SAVE_AREA_SEGMENT EsSegment;     // +0x000
    SVM_VMCB_STATE_SAVE_AREA_SEGMENT CsSegment;     // +0x010
    SVM_VMCB_STATE_SAVE_AREA_SEGMENT SsSegment;     // +0x020
    SVM_VMCB_STATE_SAVE_AREA_SEGMENT DsSegment;     // +0x030
    SVM_VMCB_STATE_SAVE_AREA_SEGMENT FsSegment;     // +0x040
    SVM_VMCB_STATE_SAVE_AREA_SEGMENT GsSegment;     // +0x050
    SVM_VMCB_STATE_SAVE_AREA_SEGMENT GdtrSegment;   // +0x060
    SVM_VMCB_STATE_SAVE_AREA_SEGMENT LdtrSegment;   // +0x070
    SVM_VMCB_STATE_SAVE_AREA_SEGMENT IdtrSegment;   // +0x080
    SVM_VMCB_STATE_SAVE_AREA_SEGMENT TrSegment;     // +0x090
    UINT8 Reserved1[0x0cb - 0x0a0];             // +0x0a0
    UINT8 Cpl;                                  // +0x0cb
    UINT32 Reserved2;                           // +0x0cc
    UINT64 Efer;                                // +0x0d0
    UINT8 Reserved3[0x148 - 0x0d8];             // +0x0d8
    UINT64 Cr4;                                 // +0x148
    UINT64 Cr3;                                 // +0x150
    UINT64 Cr0;                                 // +0x158
    UINT64 Dr7;                                 // +0x160
    UINT64 Dr6;                                 // +0x168
    UINT64 Rflags;                              // +0x170
    UINT64 Rip;                                 // +0x178
    UINT8 Reserved4[0x1d8 - 0x180];             // +0x180
    UINT64 Rsp;                                 // +0x1d8
    UINT8 Reserved5[0x1f8 - 0x1e0];             // +0x1e0
    UINT64 Rax;                                 // +0x1f8
    UINT64 Star;                                // +0x200
    UINT64 LStar;                               // +0x208
    UINT64 CStar;                               // +0x210
    UINT64 SfMask;                              // +0x218
    UINT64 KernelGsBase;                        // +0x220
    UINT64 SysenterCs;                          // +0x228
    UINT64 SysenterEsp;                         // +0x230
    UINT64 SysenterEip;                         // +0x238
    UINT64 Cr2;                                 // +0x240
    UINT8 Reserved6[0x268 - 0x248];             // +0x248
    UINT64 GPat;                                // +0x268
    UINT64 DbgCtl;                              // +0x270
    UINT64 BrFrom;                              // +0x278
    UINT64 BrTo;                                // +0x280
    UINT64 LastExcepFrom;                       // +0x288
    UINT64 LastExcepTo;                         // +0x290
} SVM_VMCB_STATE_SAVE_AREA, * PSVM_VMCB_STATE_SAVE_AREA;
static_assert(sizeof(SVM_VMCB_STATE_SAVE_AREA) == 0x298, "sizeof(SVM_VMCB_STATE_SAVE_AREA) != 0x298");

typedef struct _SVM_VMCB
{
    SVM_VMCB_CONTROL_AREA ControlArea;
    SVM_VMCB_STATE_SAVE_AREA StateSaveArea;
    UINT8 Reserved1[0x1000 - sizeof(SVM_VMCB_CONTROL_AREA) - sizeof(SVM_VMCB_STATE_SAVE_AREA)];
} SVM_VMCB, * PSVM_VMCB;
static_assert(sizeof(SVM_VMCB) == 0x1000, "VMCB not align 4KB");

typedef struct _SVM_GUEST_FRAME{
    M128A xmm0;
    M128A xmm1;
    M128A xmm2;
    M128A xmm3;
    M128A xmm4;
    M128A xmm5;
    UINT64 rax;
    UINT64 rcx;
    UINT64 rbx;
    UINT64 rdx;
    UINT64 rsi;
    UINT64 rdi;
    UINT64 rbp;
    UINT64 r8 ;
    UINT64 r9 ;
    UINT64 r10;
    UINT64 r11;
    UINT64 r12;
    UINT64 r13;
    UINT64 r14;
    UINT64 r15;
    UINT64 RFlag;
}SVM_GUEST_FRAME, *PSVM_GUEST_FRAME;
static_assert(sizeof(SVM_GUEST_FRAME) == 0xe0, "sizeof(GUEST_FRAME) != 0xe0");

struct _SVM_CONFIG;

typedef struct _SVM_VMMSTACK
{
    SVM_GUEST_FRAME Frame;
    struct _SVM_CONFIG* VmConfig;
    PHYSICAL_ADDRESS CurrentVmcbPA;
}SVM_VMM_STACK, *PSVM_VMM_STACK;
static_assert(offsetof(SVM_VMM_STACK, VmConfig) == 0xe0, "offsetof(SVM_VMM_STACK, VmConfig) != 0xe0");
static_assert(sizeof(SVM_VMM_STACK) == 0xf0, "sizeof(SVM_VMM_STACK) != 0xf0");

typedef VOID(SVM_VM_EXIT_HANDLER)(PSVM_VMM_STACK VmStack);
typedef SVM_VM_EXIT_HANDLER* PSVM_VM_EXIT_HANDLER;

typedef struct _SVM_GLOBAL_CONFIG {
    UINT32 GuestAsid;
    SIZE_T VmmStackSize;
    PSVM_VM_EXIT_HANDLER VMExitFunction;
    struct _SVM_CONFIG* ConfigList;
    SIZE_T ConfigActiveCount;
    SIZE_T ConfigCount;
}SVM_GLOBAL_CONFIG, *PSVM_GLOBAL_CONFIG;

typedef struct _SVM_CONFIG {
    PSVM_VM_EXIT_HANDLER VmExitFunction;
    PHYSICAL_ADDRESS VmcbPA;            // 在此之前的成员位置与大小不能发生改变，有汇编引用！
    PSVM_VMCB Vmcb;
    PVOID VmHostSave;
    PHYSICAL_ADDRESS VmHostSavePA;
    PVOID VmmStackBase;
    PVOID VmmStackTop;
    SIZE_T VmmStackSize;
    PSVM_GLOBAL_CONFIG GlobalConfig;
}SVM_CONFIG, *PSVM_CONFIG;
static_assert(offsetof(SVM_CONFIG, Vmcb) == 0x10, "offsetof(SVM_CONFIG, Vmcb) != 0x10");

#pragma pack(pop)